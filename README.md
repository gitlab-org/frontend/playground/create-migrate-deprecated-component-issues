> Disclaimer: This script has been abstracted into a GUI, consider using it instead.
> https://gitlab.com/gitlab-org/frontend/playground/batch-issue-creator

This script creates issues to document files where we have to replace `<gl-deprecated-button>`
with `<gl-button>`.

## Usage

```bash
./index.js --projectId [project-id] --apiToken [apiToken] --epicId [epicId] [file.json]
```

### Parameters and arguments

- projectId: ID of the project where the issues will be created.
- apiToken: OAuth API token used to authenticate with  the GitLab API.
- epicId: ID of the epic where the issues will be collected (Optional).

[file.json] JSON file that contains an array with the files that contain instances of
`<gl-deprecated-button>`. For example:

```json
[
  "app/assets/javascripts/ide/components/ide.vue"
]
```

## Running the migration script

> **Note:**
> * Before running the script, make sure to commit/stash your changes!
> * While this migration script is able to migrate most deprecated button usages, it is not smart
> enough to migrate deprecated instances when `GlDeprecatedButton` has been imported as `GlButton`,
> or to migrate dynamically bound `variant` props.
> Also, buttons that already have a `category` set might not be migrated properly, the script should
> warn you about those occurrences for you to verify the changes.

- Clone this repository locally and `cd` into its directory.
- Run the migration script by passing the file or directory to be migrated as the first argument:

```sh
./migrate ../gdk/gitlab/app/assets/javascripts/
./migrate ../gdk/gitlab/app/assets/javascripts/some/file.vue
```

- The script automatically prettifies and stages your changes. Double-check that the changes look
  good, then commit, push and open a Merge Request!
