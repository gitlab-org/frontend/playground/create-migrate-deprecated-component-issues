const glob = require("fast-glob");
const { readFileSync, writeFileSync } = require("fs");

const VARIANT_PATTERN = /(?<dynamicBinding>:?)variant="(?<variantValue>[^"]+)"/;
const CATEGORY_PATTERN = /category="/;
let migratedFiles = 0;

const variantsMap = {
  primary: {
    variant: "info",
    category: "primary",
  },
  secondary: {},
  dark: {},
  light: {},
  link: {
    variant: "link",
  },
  success: {
    variant: "success",
    category: "primary",
  },
  "outline-success": {
    variant: "success",
    category: "secondary",
  },
  info: {
    variant: "info",
    category: "primary",
  },
  "outline-info": {
    variant: "info",
    category: "secondary",
  },
  warning: {
    variant: "warning",
    category: "primary",
  },
  "outline-warning": {
    variant: "warning",
    category: "secondary",
  },
  danger: {
    variant: "danger",
    category: "primary",
  },
  "outline-danger": {
    variant: "danger",
    category: "secondary",
  },
};

async function migrateFile(file) {
  const bytes = await readFileSync(file);
  const contents = bytes.toString();
  let isDeprecatedButton = false;
  const updatedContents = contents
    .split("\n")
    .map((line, index) => {
      let updatedLine = line;
      updatedLine = line.replace("GlDeprecatedButton", "GlButton");
      if (/<gl-deprecated-button/.test(line)) {
        isDeprecatedButton = true;
      }
      const variantMatch = VARIANT_PATTERN.exec(line);
      const categoryMatch = CATEGORY_PATTERN.exec(line);

      if (isDeprecatedButton && categoryMatch) {
        console.warn(
          `\x1b[43m\x1b[30mbutton has a category set in ${file}:${
            index + 1
          } and might end up with a duplicated category prop after the migration, please double-check this change\x1b[0m`
        );
      }

      if (isDeprecatedButton && variantMatch) {
        const {
          groups: { dynamicBinding, variantValue },
        } = variantMatch;
        if (dynamicBinding) {
          console.warn(
            `\x1b[41m\x1b[37mvariant is bound dynamically in ${file}:${
              index + 1
            }, please migrate it manually\x1b[0m`
          );
        } else {
          const newValues = variantsMap[variantValue];
          if (newValues) {
            const newValuesStr = Object.entries(newValues)
              .map(([prop, value]) => `${prop}="${value}"`)
              .join(" ");
            updatedLine = updatedLine.replace(VARIANT_PATTERN, newValuesStr);
          }
        }
      }
      if (isDeprecatedButton && line.includes("</gl-deprecated-button")) {
        isDeprecatedButton = false;
      }
      updatedLine = updatedLine.replace(/gl-deprecated-button/g, "gl-button");
      return updatedLine;
    })
    .join("\n");
  if (updatedContents !== contents) {
    migratedFiles += 1;
    writeFileSync(file, updatedContents);
  }
}

async function migrateDir(dir) {
  const globPattern = `${dir}/**/*.{vue,js}`;
  const files = await glob(globPattern);
  console.log(`Matched ${files.length} files`);
  for await (file of files) {
    migrateFile(file);
  }
}

async function main() {
  if (process.argv.length < 3) {
    throw new Error("No directory provided");
  } else {
    const path = process.argv[2];
    if (path.endsWith(".js") || path.endsWith(".vue")) {
      await migrateFile(path);
    } else {
      await migrateDir(path);
    }
    console.log(`Migrated ${migratedFiles} files`);
  }
}

main();
