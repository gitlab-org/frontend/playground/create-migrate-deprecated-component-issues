## Instructions

- [ ] Using the [migration script](https://gitlab.com/gitlab-org/frontend/playground/create-migrate-deprecated-component-issues/-/blob/master/README.md#running-the-migration-script), or by manually editing
 the files:
  - [ ] Replace each instance of `<gl-deprecated-button>`  with `<gl-button>`.
  - [ ] Convert each variant attribute to the new variant/category combination
using the variant mapping guide.
- [ ] Take before/after screenshots of each button in the file, and upload them
in the Merge Request description.
- [ ] Request review from `@dimitrieh` or `@tauriedavis`.

### Variant mapping guide

- `variant="primary"`          -> `variant="info" category="primary"`
- `variant="secondary"`        -> remove attribute
- `variant="dark"`             -> remove attribute
- `variant="light"`            -> remove attribute
- `variant="link"`             -> keep attribute
- `variant="success"`          -> `variant="success" category="primary"`
- `variant="outline-success"`  -> `variant="success" category="secondary"`
- `variant="info"`             -> `variant="info" category="primary"`
- `variant="outline-info"`     -> `variant="info" category="secondary"`
- `variant="warning"`          -> `variant="warning" category="primary"`
- `variant="outline-warning"`  -> `variant="warning" category="secondary"`
- `variant="danger"`           -> `variant="danger" category="primary"`
- `variant="outline-danger"`   -> `variant="danger" category="secondary"`

### Examples

```patch
- <gl-deprecated-button variant="outline-success" />
+ <gl-button variant="success" category="secondary" />
```

```patch
- <gl-deprecated-button variant="primary" />
+ <gl-button />
```

```patch
- <gl-deprecated-button variant="secondary" />
+ <gl-button />
```

```patch
- <gl-deprecated-button variant="success" />
+ <gl-button variant="success" category="primary" />
```
