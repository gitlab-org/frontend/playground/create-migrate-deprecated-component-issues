#!/usr/bin/env node

const fs = require('fs');
const axios = require('axios');
const minimist = require('minimist');

const { projectId, apiToken, epicId, _: [filesPath] } = minimist(process.argv.slice(2));
const files = require(filesPath);
const description = fs.readFileSync('./issue-description.md', { encoding: 'utf-8' });

axios.defaults.headers.common['Private-Token'] = apiToken;

const API_URL = `https://gitlab.com/api/v4/projects/${projectId}/issues`;

const createIssue = (file) => {
  const issuePayload = {
    title: `Replace <gl-deprecated-button> with <gl-button> in ${file}`,
    labels: 'frontend,component:button,pajamas::integrate',
    description: description,
    epic_id: epicId,
  }

  return axios.post(API_URL, issuePayload)
    .then(() => {
      console.log(`Issue for file ${file} created successfully.`);
    })
    .catch(({ response: { statusText, data }}) => {
      console.error(`An error occurred creating issue for file ${file}.`, statusText, data);
      throw new Error();
    });
}

const createIssues = () => Promise.all(files.map(createIssue));

createIssues()
  .then(() => {
    console.log('All issues were created successfully.')
  })
  .catch(() => {
    console.error('One or more issues could not be created.');
  });
